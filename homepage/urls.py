from django.urls import path
from .views import index, data

app_name = 'homepage'

urlpatterns = [
    path('', index, name='index'),
    path('data/', data, name='data'),
]