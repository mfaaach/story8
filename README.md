# Pow's Library

[![Pipeline](https://gitlab.com/mfaaach/story8/badges/master/pipeline.svg)](https://gitlab.com/mfaaach/story8/pipelines)
[![coverage report](https://gitlab.com/mfaaach/story8/badges/master/coverage.svg)](https://gitlab.com/mfaaach/story8/commits/master)


Fachri's PPW Lab 8 Project 

## URL

This lab projects can be accessed from [https://powlibrary.herokuapp.com](https://powlibrary.herokuapp.com)

## Authors

* **Muhammad Fachri Anandito** - [mfaaach](https://gitlab.com/mfaaach)
